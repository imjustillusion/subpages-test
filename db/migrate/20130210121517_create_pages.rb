class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :pagename
      t.string :pagetitle
      t.text :content
      t.integer :parent_id

      t.timestamps
    end
  end
end
