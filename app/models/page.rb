class Page < ActiveRecord::Base
  attr_accessible :content, :pagename, :pagetitle, :parent_id

  has_many :subpages, class_name: "Page", foreign_key: 'parent_id'
  belongs_to :parent, class_name: "Page", foreign_key: 'parent_id'
  #валидация урла.
  validates :pagename, presence: {:on => :create},  uniqueness: {:on => :create}, format: { :with => /\A[[[:alnum:]]_\/]+\z/}
  validates_presence_of  :pagetitle

end
