class PagesController < ApplicationController

  #получаем страницу по ее адресу.
  before_filter :get_page_from_address, only: [:show, :add, :edit]

  # *page GET
  def show
    # если страница не найдена, отдаем соответствующй текст и статус.
      unless @page
        render file: 'public/404.html', status: 404 #'page not found', status: 404 #params.inspect
      else
    end
  end

  # *page/edit GET
  def edit
  
  end

  # /update PUT
  def update
    #обновляем ресурс
    @page = Page.find(params[:id])
    @page.update_attributes(params[:page])
    if @page.errors.empty?
      #если страница главная, отправляем в корень, в противном случае на отредактированную страницу
      unless @page.pagename == 'index'
        redirect_to "/#{@page.pagename}"
      else
        redirect_to "/"
      end
    else
      render text: "edit"
    end
  end

  # добавлеие страницы
  # *page/add GET
  def add
  end

  # *page/add POST
  def create
  # если создаем страницу не в корне, то необходимо в pagename добавить адрес родительской страницы.
    if (params[:page][:parent_id])
      parent_path = Page.where("id = ?",params[:page][:parent_id]).pluck(:pagename).first
      params[:page][:pagename] = "#{parent_path}/#{params[:page][:pagename]}"
    end
    #создаем новю страницу
    @page = Page.create(params[:page])
    if @page.errors.empty?
      #редиректим на вновь созданную страницу
      redirect_to "/#{@page.pagename}"
     else
      #если есть ошибки, то возвращаемся к форме редактировния
      redirect_to request.referer #"/#{parms}"
    end
  end

  private
  # поиск страниц по адресу
  def get_page_from_address
    @page = Page.find_by_pagename(params[:page_path])
  end
end
